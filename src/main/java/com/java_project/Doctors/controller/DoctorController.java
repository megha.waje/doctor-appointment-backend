package com.java_project.Doctors.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.java_project.Doctors.model.Doctor;
import com.java_project.Doctors.model.Patient;
import com.java_project.Doctors.model.Review;

import org.springframework.web.bind.annotation.RequestMapping;
import com.java_project.Doctors.repository.DoctorRepository;
import com.java_project.Doctors.repository.PatientRepository;
import com.java_project.Doctors.repository.ReviewRepository;
import com.java_project.Doctors.repository.AppointmentRepository;
import com.java_project.Doctors.model.Appointment;

@RestController
@RequestMapping("/api/v1/")
public class DoctorController {
	
	@Autowired
	private DoctorRepository doctorRepository;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/doctors")
	public List<Doctor> getAllDoctor(){
		return doctorRepository.findAll();
	}
	
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/doctors/{id}/appointments")
	public List<Appointment> getAppointmentsByDrId(@PathVariable("id") int doctorId){
		List<Appointment> result = new ArrayList<Appointment>();
		List<Appointment> appointments = appointmentRepository.findAll();

        for (Appointment appoinment: appointments) {

            if (appoinment.getDap_id() == doctorId) {

                result.add(appoinment);
            }
        }
		return result;
	}
	

	@Autowired
	private ReviewRepository reviewRepository;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/doctors/{id}/reviews")
	public List<Review> getReviewById(@PathVariable("id") int docId){
		List<Review> results = new ArrayList<Review>();
		List<Review> reviews = reviewRepository.findAll();

        for (Review review: reviews) {

            if (review.getDr_id() == docId) {

                results.add(review);
            }
        }
		return results;
	 }
	
	
	@CrossOrigin(origins = "*")
	@PostMapping("/add-review")
	public Review saveReview(@RequestBody Review review) {
		return reviewRepository.save(review);
	}
	
	
	@CrossOrigin(origins = "*")
	@PutMapping("/doctors/{id}/book-appointment")
    public ResponseEntity<Object> updateAppoinment(@RequestBody Appointment apt, @PathVariable int id) {
		
		 Appointment tmpApt = null;

        List<Appointment> appointmentList = appointmentRepository.findAll();
        
        for (Appointment currentApt: appointmentList ) {
        	
            if (currentApt.getDap_id() == apt.getDap_id() && currentApt.getId() == apt.getId()) {
            	
            	currentApt.setStatus("booked");
            	tmpApt = currentApt;
            	
            }
           
        }
        
        appointmentRepository.save(tmpApt);
        return ResponseEntity.noContent().build();
    }
	
	
	
	@Autowired
	private PatientRepository patientRepository;
	
	@CrossOrigin(origins = "*")
	@PostMapping("/add-patient")
	public Patient savePatient(@RequestBody Patient patient) {
		return patientRepository.save(patient);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/patients")
	public List<Patient> getAllPatient(){
		return patientRepository.findAll();
	}
	

}