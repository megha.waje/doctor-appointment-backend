package com.java_project.Doctors.model;
import javax.persistence.*;

@Entity
@Table (name = "appointment") 
public class Appointment {
	
	@Id
	@GeneratedValue
	private int id;
	
	
	private String date;
	private String start_time;
	private String end_time;
	private String status;
	private String ap_id;
	private int dap_id;
	
	
    public Appointment() {
		
	}
	
	
	public Appointment(int id, String date, String start_time, String end_time, String status, String ap_id, int dap_id) {
		super();
		this.id = id;
		this.date = date;
		this.start_time = start_time;
		this.end_time = end_time;
		this.status = status;
		this.ap_id = ap_id;
		this.dap_id = dap_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAp_id() {
		return ap_id;
	}
	public void setAp_id(String ap_id) {
		this.ap_id = ap_id;
	}
	public int getDap_id() {
		return dap_id;
	}
	public void setDap_id(int dap_id) {
		this.dap_id = dap_id;
	}
	
	
}
