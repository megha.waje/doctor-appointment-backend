package com.java_project.Doctors.model;
import javax.persistence.*;

@Entity
@Table (name = "doc_availability")
public class Availability {
	
	@Id
	private int id;
	
	private char is_available;
	private int da_id;
	
	public Availability() {
		
	}

	public Availability(int id, char is_available, int da_id) {
		super();
		this.id = id;
		this.is_available = is_available;
		this.da_id = da_id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public char getIs_available() {
		return is_available;
	}
	public void setIs_available(char is_available) {
		this.is_available = is_available;
	}
	public int getDa_id() {
		return da_id;
	}
	public void setDa_id(int da_id) {
		this.da_id = da_id;
	}
	
	

}
