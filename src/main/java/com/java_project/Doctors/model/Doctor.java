package com.java_project.Doctors.model;

import javax.persistence.*;
import java.util.List;


@Entity
@Table (name= "doctor")


public class Doctor {
	@Id
	private int id;
	
	@OneToMany(targetEntity = Appointment.class,cascade = CascadeType.ALL)
	@JoinColumn(name ="dap_id",referencedColumnName = "id")
	private List<Appointment> appointments;
	
	//@ElementCollection // 1
    //@CollectionTable(name = "Appointment", joinColumns = @JoinColumn(name = "dap_id")) // 2
    //@Column(name = "id") // 3
	private String fname;
	private String lname;
	private String qualification;
	private String speciality;
	private String gender;
	private String location;
	private float fees;
	private String services;
	
	
	public Doctor() {
		
	}
	
    

	

	public Doctor(int id, List<Appointment> appointments, String fname, String lname, String qualification,
			String speciality, String gender, String location, float fees, String services) {
		super();
		this.id = id;
		this.appointments = appointments;
		this.fname = fname;
		this.lname = lname;
		this.qualification = qualification;
		this.speciality = speciality;
		this.gender = gender;
		this.location = location;
		this.fees = fees;
		this.services = services;
	}





	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}




	public String getLocation() {
		return location;
	}




	public void setLocation(String location) {
		this.location = location;
	}




	public float getFees() {
		return fees;
	}




	public void setFees(float fees) {
		this.fees = fees;
	}



	public String getServices() {
		return services;
	}



	public void setServices(String services) {
		this.services = services;
	}
	
	
	

}
