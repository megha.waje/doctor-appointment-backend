package com.java_project.Doctors.model;

import javax.persistence.*;

@Entity
@Table (name = "review")
public class Review {
	
	@Id
	@GeneratedValue
	private int id;
	
	private String review;
	private int rating;
	private int dr_id;
	private String name;
	
	public Review() {
		
	}
	
	
	
	



	public Review(int id, String review, int rating, int dr_id, String name) {
		super();
		this.id = id;
		this.review = review;
		this.rating = rating;
		this.dr_id = dr_id;
		this.name = name;
	}







	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getDr_id() {
		return dr_id;
	}
	public void setDr_id(int dr_id) {
		this.dr_id = dr_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
