package com.java_project.Doctors.model;
import javax.persistence.*;

@Entity
@Table (name = "patient")
public class Patient {
	
	@Id
	@GeneratedValue
	private int id;
	
	
	private String p_fname;
	private String p_lname;
	private String contact;
	private String disease_name;
	private int dp_id;
	private String symptom1;
	private String symptom2;
	private String gender;
	
    public Patient() {
		
	}
	
    
	
	



	public Patient(int id, String p_fname, String p_lname, String contact, String disease_name, int dp_id,
			String symptom1, String symptom2, String gender) {
		super();
		this.id = id;
		this.p_fname = p_fname;
		this.p_lname = p_lname;
		this.contact = contact;
		this.disease_name = disease_name;
		this.dp_id = dp_id;
		this.symptom1 = symptom1;
		this.symptom2 = symptom2;
		this.gender = gender;
	}







	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getP_fname() {
		return p_fname;
	}
	public void setP_fname(String p_fname) {
		this.p_fname = p_fname;
	}
	public String getP_lname() {
		return p_lname;
	}
	public void setP_lname(String p_lname) {
		this.p_lname = p_lname;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDisease_name() {
		return disease_name;
	}
	public void setDisease_name(String disease_name) {
		this.disease_name = disease_name;
	}
	public String getSymptom1() {
		return symptom1;
	}
	public void setSymptom1(String symptom1) {
		this.symptom1 = symptom1;
	}
	public String getSymptom2() {
		return symptom2;
	}
	public void setSymptom2(String symptom2) {
		this.symptom2 = symptom2;
	}

	public int getDp_id() {
		return dp_id;
	}

	public void setDp_id(int dp_id) {
		this.dp_id = dp_id;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}
	
	

}
