package com.java_project.Doctors.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.java_project.Doctors.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

}
