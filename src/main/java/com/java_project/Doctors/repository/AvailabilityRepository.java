package com.java_project.Doctors.repository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.java_project.Doctors.model.Availability;

@Repository
public interface AvailabilityRepository extends JpaRepository<Availability, Integer>{

}
